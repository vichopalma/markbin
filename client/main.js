import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import App from './components/app';
import BinsMain from './components/bins/bins_main';
import BinsList from './components/bins/bins_list';
import BinsWicked from './components/bins/bins_wicked';
import { Bins } from '../imports/collections/bins';

//si se agrega "exact" antes del path, redenriza el primer
//elemento solo si el path es el exacto.
const routes = (
  //v4
  <Router>
    <div>
      <App/>
      <Switch>
        <Route exact path="/" component={BinsList} />
        <Route path="/bins/:binId" component={BinsMain} />
        <Route path="/wicked" component={BinsWicked} />
      </Switch>
    </div>
  </Router>
);

Meteor.startup(() => {
  ReactDOM.render(routes, document.querySelector('.render-target'));
});
