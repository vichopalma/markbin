import React, { Component } from 'react';
import { markdown } from 'markdown';

class BinsViewer extends Component {
  render() {

    const rawHTM = markdown.toHTML(this.props.bin.content);

    return(
      <div className="col-xs-4">
        <h5>Output</h5>
        <div dangerouslySetInnerHTML={{ __html: rawHTM}} ></div>
      </div>
    );
  }
}

export default BinsViewer;
