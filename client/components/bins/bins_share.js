import React, { Component } from 'react';

class BinsShare extends Component {

  onShareClick() {
    const email = this.refs.email.value;

    Meteor.call('bins.share', this.props.bin, email);
  }

  onShareRemove() {
    const email = this.refs.email.value;
    //console.log(this.props.bin);
    //console.log(this.refs);
    Meteor.call('bins.removeShare', this.props.bin, email);
  }

  renderShareList() {
      return this.props.bin.sharedWith.map(email => {
        return (
          <button
            onClick={this.onShareRemove.bind(this)}
            key={email}
            className="btn btn-default">
            {email}
          </button>
        );
      });
  }

  render() {
    return(
      <footer className="bins-share">
        <div className="input-group">
          <input ref="email" className="form-control" />
          <div className="input-group-btn">
            <button
              onClick={this.onShareClick.bind(this)}
              className="btn btn-default">
              Share Bin
            </button>
          </div>
        </div>
        <div className="btn-group">
          Shared With:
        </div>
        <div>
          {this.renderShareList()}
        </div>
      </footer>
    );
  }
}

export default BinsShare;
